package com.lututong.uarttools;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

/**
 * Created by Administrator on 2018/5/29.
 */

public class UartJniTool {

    private static final String TAG= "UartJniTool";
    private static final int UARTDATA= 8001;
    private Handler mhandler;

    static {
        System.loadLibrary("UartToothJni");
    }

    public UartJniTool(Handler mhandler) {
        this.mhandler = mhandler;
    }

    public void getUartDataHandler(byte[] dataBuf, int dataLen)
    {
        //Log.i(TAG, "Recv Data Len = "+dataLen);
        Message msg = new Message();
        msg.what = UARTDATA;
        Bundle bundle = new Bundle();
        bundle.putByteArray("Data Buffer", dataBuf);
        bundle.putInt("Data Length", dataLen);
        msg.setData(bundle);//mes利用Bundle传递数据
        mhandler.sendMessage(msg);//用activity中的handler发送消息
    }

    public native int uartToolStart();
    public native int uartToolStop();
    public native int StartTofHandler();
    public native int CloseTofHandler();
}
