package com.lututong.uarttools;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    private static final String TAG= "MainActivity";
    private static final int UARTDATA= 8001;
    private static final int SHOW_DATA_LENGTH= 900;

    UartJniTool  uartJniTool;
    ToggleButton uartStateBtn;
    Button startTofBtn, closeTofBtn;
    TextView uartText;
    private int offset;
    private StringBuffer showDataStrBuf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        showDataStrBuf = new StringBuffer();
        uartJniTool = new UartJniTool(mhandler);

        uartText = (TextView)findViewById(R.id.uartText);
        uartText.setMovementMethod(ScrollingMovementMethod.getInstance());

        uartStateBtn = (ToggleButton)findViewById(R.id.uartStateBtn);
        startTofBtn = (Button)findViewById(R.id.startTofBtn);
        closeTofBtn = (Button)findViewById(R.id.closeTofBtn);

        uartStateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (uartStateBtn.isChecked()){
                   int result = uartJniTool.uartToolStart();
                    if (result < 0){
                        uartStateBtn.setChecked(false);
                    }
                }else {
                    uartJniTool.uartToolStop();
                }
            }
        });

        startTofBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uartJniTool.StartTofHandler();
            }
        });

        closeTofBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uartJniTool.CloseTofHandler();
            }
        });
    }

    Handler mhandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UARTDATA:
                    String recvDataStr;
                    int deleLen;
                    byte[] dataBuf = msg.getData().getByteArray("Data Buffer");
                    int dataLen = msg.getData().getInt("Data Length");

                    recvDataStr = bytesToHex(dataBuf, dataLen);
                    Log.i(TAG, "Recv Data Len = "+dataLen);
                    Log.i(TAG, recvDataStr);

                    showDataStrBuf.append(recvDataStr);
                    if(showDataStrBuf.length() >= SHOW_DATA_LENGTH) {
                        deleLen = showDataStrBuf.length() - SHOW_DATA_LENGTH;
                        showDataStrBuf.delete(0, deleLen);
                    }
                    uartText.setText(showDataStrBuf);
                    break;
            }
            super.handleMessage(msg);
        }
    };

    public static String numToHex8(int b) {
        return String.format("0x%02x", b);//2表示需要两个16进行数
    }

    private static String bytesToHex(byte[] bytes, int len) {
        int i;
        StringBuilder sb = new StringBuilder();
        for (i = 0; i < len; i++) {
            sb.append(String.format("%02X ", bytes[i]));
        }
        return sb.toString();
    }
}
